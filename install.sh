#! /usr/bin/env bash

cargo install deno --locked
deno install -qAn vr https://deno.land/x/velociraptor@1.5.0/cli.ts
echo -e "export PATH=\"~/.deno/bin:$PATH\"" > ~/.bashrc
