import { blue, gray, green, red } from 'colours';

console.log(green(`
╔═════════════════════════════════════════╗
║  _____               __                 ║
║ |  __ \\             / _|                ║
║ | |  | | ___   ___ | |_ _   _ ___  ___  ║
║ | |  | |/ _ \\ / _ \\|  _| | | / __|/ _ \\ ║
║ | |__| | (_) | (_) | | | |_| \\__ |  __/ ║
║ |_____/ \\___/ \\___/|_|  \\__,_|___/\\___| ║
║                              v0-alpha.1 ║
╚═════════════════════════════════════════╝
`));

const server = Deno.listen({ port: 8080 });
console.log(
	green('HTTP webserver running.  Access it at:  http://localhost:8080/'),
);
console.log(
	gray('--------------------------------------------------------------'),
);

try {
	for await (const conn of server) serveHttp(conn);
} catch (err) {
	console.error(red('Couldn\'t parse http connection'));
	console.error(gray('------------------------------'));
	console.error(err);
}

function getUserAgent({ request: req }: Deno.RequestEvent): string {
	if (req.headers.get('user-agent') === null) {
		return red('unknown');
	}
	return blue(req.headers.get('user-agent') as string);
}

async function serveHttp(conn: Deno.Conn): Promise<void> {
	const httpConn = Deno.serveHttp(conn);

	for await (const requestEvent of httpConn) {
		const body = `<div style="color: blue">Your user-agent is:\n${
			requestEvent.request.headers.get('user-agent') ??
				'<span style="color: red">Unknown</span>'
		}\n</div>\n`;
		console.log(
			`${blue('Conection from:')} ${getUserAgent(requestEvent)}`,
		);
		requestEvent.respondWith(
			new Response(body, {
				status: 200,
				headers: [
					['Content-Type', 'text/html'],
				],
			}),
		);
	}
}
